﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core
{
    public enum RecordingStatus
    {
        NotReady,
        Ready,
        Recording,
        Off
    }
}
