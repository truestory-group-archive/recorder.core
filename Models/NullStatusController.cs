﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Models
{
    public class NullStatusController : IStatusController
    {
        public void SetStatus(RecordingStatus status)
        {
            // We do nothing at all...
        }
    }
}
