﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Models
{
    public class VideoDevice : IVideoDevice
    {
        public string DeviceId { get; set; }
        public string PNPDeviceId { get; set; }
        public string Name { get; set; }
    }
}
