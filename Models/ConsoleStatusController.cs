﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Models
{
    public class ConsoleStatusController : IStatusController
    {
        public void SetStatus(RecordingStatus status)
        {
            if (status == RecordingStatus.Ready)
                Console.WriteLine("Set Status: Ready");
            else if (status == RecordingStatus.NotReady)
                Console.WriteLine("Set Status: Not Ready");
            else if (status == RecordingStatus.Recording)
                Console.WriteLine("Set Status: Recording");
            else
                Console.WriteLine("Set Status: Unknown");
        }
    }
}
