﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Models
{
    public class CompositeRecorder : IRecorder
    {
        private IAudioDiscovery _audioDiscovery;
        private IVideoDiscovery _videoDiscovery;

        private IAudioDeviceFilter _audioDeviceFilter;
        private IVideoDeviceFilter _videoDeviceFilter;

        private IAudioRecorderFactory _audioRecorderFactory;
        private IVideoRecorderFactory _videoRecorderFactory;

        public CompositeRecorder(IAudioDiscovery audioDiscovery, IVideoDiscovery videoDiscovery, IAudioDeviceFilter audioDeviceFilter, IVideoDeviceFilter videoDeviceFilter, IAudioRecorderFactory audioRecorderFactory, IVideoRecorderFactory videoRecorderFactory)
        {
            _audioDiscovery = audioDiscovery;
            _videoDiscovery = videoDiscovery;

            _audioDeviceFilter = audioDeviceFilter;
            _videoDeviceFilter = videoDeviceFilter;

            _audioRecorderFactory = audioRecorderFactory;
            _videoRecorderFactory = videoRecorderFactory;
        }


        private List<IRecorder> _recorders = null;
        private IEnumerable<IRecorder> GetRecorders()
        {
            if(_recorders == null) { 
                var videoDevices = _videoDeviceFilter.Filter(_videoDiscovery.GetDevices());
                var audioDevices = _audioDeviceFilter.Filter(_audioDiscovery.GetDevices());

                _recorders = new List<IRecorder>();

                foreach (var d in videoDevices)
                    _recorders.Add(_videoRecorderFactory.Create(d));
                foreach (var d in audioDevices)
                    _recorders.Add(_audioRecorderFactory.Create(d));
            }

            if (!_recorders.Any())
                _status = RecordingStatus.NotReady;
            else
                _status = RecordingStatus.Ready;

            return _recorders;
        }

        public void Start()
        {
            var recorders = GetRecorders();

            if (_status == RecordingStatus.NotReady)
                return;

            foreach (var r in recorders)
                if(r.Status() == RecordingStatus.Ready)
                    r.Start();

            _status = RecordingStatus.Recording;
        }

        private RecordingStatus _status = RecordingStatus.NotReady;
        public RecordingStatus Status()
        {
            if(_status == RecordingStatus.NotReady)
            {
                GetRecorders();
            }
            
            return _status;
        }

        public void Stop()
        {
            var recorders = GetRecorders();

            if (_status == RecordingStatus.NotReady)
                return;

            foreach (var r in recorders)
                if(r.Status() == RecordingStatus.Recording)
                    r.Stop();
            _status = RecordingStatus.Ready;
        }
    }
}
