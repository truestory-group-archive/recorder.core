﻿using Recorder.Core.Interfaces;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core
{
    public class Bootstrapper : IBootstrapper
    {
        private Container _container;

        public delegate void RunEventHandler(object sender, EventArgs e);
        public delegate void ExitEventHandler(object sender, EventArgs e);

        public Container Initialize()
        {
            // 1. Create a new Simple Injector container
            _container = new Container();
            _container.Options.AllowOverridingRegistrations = true;

            var self = this;
            _container.Register<IBootstrapper>(() => {
                return self;
            }, Lifestyle.Singleton);

            _container.Register<ILogger, Recorder.Core.Models.ConsoleLogger>(Lifestyle.Singleton);
            _container.Register<IRecordingService, Recorder.Core.DefaultRecordingService>(Lifestyle.Singleton);
            _container.Register<IRecorder, Recorder.Core.Models.CompositeRecorder>(Lifestyle.Singleton);

            var registration = Lifestyle.Singleton.CreateRegistration<DeviceFilter>(_container);
            _container.AddRegistration(typeof(IVideoDeviceFilter), registration);
            _container.AddRegistration(typeof(IAudioDeviceFilter), registration);
            

            return _container;
        }

        public event RunEventHandler Running;
        public void Run()
        {

        }

        public event ExitEventHandler Exiting;

        public void Exit()
        {
            Exiting(this, null);
        }
    }
}
