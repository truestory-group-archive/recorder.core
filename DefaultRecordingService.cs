﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core
{
    public class DefaultRecordingService : IRecordingService
    {
        private IRecorder _recorder;
        private ILogger _logger;
        private IEnumerable<IStatusController> _statusControllers;
        private RecordingStatus _status = RecordingStatus.NotReady;

        public DefaultRecordingService(IRecorder recorder, IEnumerable<IStatusController> statusControllers, ILogger logger)
        {
            _logger = logger;
            _recorder = recorder;
            _statusControllers = statusControllers;

            SetStatus(recorder.Status());
        }

        public void Start()
        {
            _logger.Log("Starting recording");
            _recorder.Start();

            SetStatus(_recorder.Status());
        }

        public void Stop()
        {
            _logger.Log("Stopping recording");
            _recorder.Stop();
            SetStatus(_recorder.Status());
        }

        private void SetStatus(RecordingStatus status)
        {
            _status = status;
            _logger.Log("Set status: " + status.ToString());
            foreach (var statusController in _statusControllers)
                statusController.SetStatus(status);
        }
    }
}
