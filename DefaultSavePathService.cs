﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Recorder.Core
{
    public class DefaultSavePathService : ISavePathService
    {
        private string _prefix;
        public DefaultSavePathService(string savePathPrefix)
        {
            _prefix = savePathPrefix;
        }

        public string GetPath(IDevice device, string extension)
        {
            var date = DateTime.Now.ToString("yyyy-MM-dd");
            var deviceName = Sanitize(device.Name);

            var path = System.IO.Path.Combine(_prefix, string.Format("{0}-{1}.{2}", date, deviceName, extension));
            var count = 1;
            while (System.IO.File.Exists(path))
            {
                path = System.IO.Path.Combine(_prefix, string.Format("{0}-{1}-{2}.{3}", date, deviceName, count, extension));
                count++;
            }

            return path;
        }

        private string Sanitize(string name)
        {
            Regex regex = new Regex("[^a-z0-9 -]");
            name = name.Replace(' ', '-').ToLower();

            return regex.Replace(name, "");
        }
    }
}
