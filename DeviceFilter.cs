﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core
{
    public class DeviceFilter : IVideoDeviceFilter, IAudioDeviceFilter
    {
        public IEnumerable<IAudioDevice> Filter(IEnumerable<IAudioDevice> devices)
        {
            return devices;
        }

        public IEnumerable<IVideoDevice> Filter(IEnumerable<IVideoDevice> devices)
        {
            return devices;
        }
    }
}
