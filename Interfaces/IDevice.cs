﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Interfaces
{
    public interface IDevice
    {
        string Name { get; set; }
        string DeviceId { get; set; }
        string PNPDeviceId { get; set; }
    }
}
