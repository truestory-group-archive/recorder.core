﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Interfaces
{
    public interface IStatusController
    {
        void SetStatus(RecordingStatus status);
    }
}
