﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Interfaces
{
    public interface IBootstrapper
    {
        Container Initialize();
        void Run();
        void Exit();

        event Bootstrapper.RunEventHandler Running;
        event Bootstrapper.ExitEventHandler Exiting;
    }
}