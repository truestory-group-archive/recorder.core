﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Core.Interfaces
{
    public interface ISavePathService
    {
        string GetPath(IDevice device, string extension);
    }
}
